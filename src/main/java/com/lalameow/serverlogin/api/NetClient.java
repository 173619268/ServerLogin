package com.lalameow.serverlogin.api;

import com.lalameow.packet.packImpl.RegistPack;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/7
 * 时间: 16:27
 * 功能：请进行修改
 */
public interface NetClient {
    void initClient();
    void connectionServer();
    void getPublicKey();
    void login(String username,String password);
    void regist(RegistPack registPack);
    void forgetPwd();
    void updatePwd(String username,String oldPwd,String newPwd);
}
