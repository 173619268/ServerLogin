package com.lalameow.serverlogin.client.config;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

/**
 * Author: SettingDust.
 * Date: 2018/4/16.
 */
public class ConfigManager {
    private static Configuration config;

    private static Logger logger;

    public static String username;
    public static String loginGateHost;
    public static int loginGatePort;

    public ConfigManager(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        config = new Configuration(event.getSuggestedConfigurationFile());
        config.load();
        load();
    }

    public static void load() {
        logger.debug("Started loading config. ");
        username = config.get(Configuration.CATEGORY_GENERAL, "username", "").getString();
        loginGateHost = config.get(Configuration.CATEGORY_GENERAL, "host", "127.0.0.1").getString();
        loginGatePort = config.get(Configuration.CATEGORY_GENERAL, "port", 7890).getInt();

        config.save();
        logger.debug("Finished loading config. ");
    }

    public static void save() {
        logger.debug("Save config. ");
        config.get(Configuration.CATEGORY_GENERAL, "username", "").set(username);
        config.get(Configuration.CATEGORY_GENERAL, "host", "127.0.0.1").set(loginGateHost);
        config.get(Configuration.CATEGORY_GENERAL, "port", 7890).set(loginGatePort);
        config.save();
        logger.debug("Finished saving config. ");
    }

    public static Logger logger() {
        return logger;
    }
}
