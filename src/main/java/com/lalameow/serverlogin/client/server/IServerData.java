package com.lalameow.serverlogin.client.server;

import net.minecraft.client.multiplayer.ServerData;

/**
 * Author: SettingDust.
 * Date: 2018/4/18.
 */
public class IServerData extends ServerData {
    private String iconUrl;

    public IServerData(String name, String ip, boolean isLan, String iconUrl) {
        super(name, ip, isLan);
        this.iconUrl = iconUrl;
    }

    public IServerData(String name, String ip, boolean isLan) {
        this(name, ip, isLan, "");
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
