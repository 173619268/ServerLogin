package com.lalameow.serverlogin.client;

import com.lalameow.packet.BasePack;
import com.lalameow.serverlogin.client.config.ConfigManager;
import com.lalameow.serverlogin.client.gui.handler.IGuiHandler;
import com.lalameow.serverlogin.common.CommonProxy;
import com.lalameow.serverlogin.network.MainClient;
import com.lalameow.serverlogin.network.McLoginClient;
import com.lalameow.serverlogin.util.ModUitls;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.security.Key;
import java.security.PublicKey;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Author: SettingDust.
 * Date: 2018/4/6.
 */
@SideOnly(Side.CLIENT)
public class ClientProxy extends CommonProxy {
    public McLoginClient mcLoginClient = new McLoginClient();
    public LinkedBlockingQueue<BasePack> packQueue = new LinkedBlockingQueue<>();
    public static PublicKey publicKey;
    public static String clientId;
    public void preInit(FMLPreInitializationEvent event) {
        new ConfigManager(event);
        MinecraftForge.EVENT_BUS.register(new IGuiHandler());
    }

    public void init(FMLInitializationEvent event) {
        ModUitls.scanFile();
    }

    public void postInit(FMLPostInitializationEvent event) {

        MainClient mainClient = new MainClient(mcLoginClient);
        new Thread(mainClient).start();

    }
}
