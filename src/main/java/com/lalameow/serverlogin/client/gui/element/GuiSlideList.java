package com.lalameow.serverlogin.client.gui.element;

import com.google.common.collect.Lists;
import net.minecraft.client.gui.Gui;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

/**
 * Author: SettingDust.
 * Date: 2018/4/20.
 */
//TODO 暂时空置
@SideOnly(Side.CLIENT)
public class GuiSlideList extends Gui {
    private List<IGuiListEntry> entries = Lists.newArrayList();

    public void drawList() {

    }

    @SideOnly(Side.CLIENT)
    public interface IGuiListEntry {
        void updatePosition(int slotIndex, int x, int y, float partialTicks);

        void drawEntry(int slotIndex, int x, int y, int listWidth, int slotHeight, int mouseX, int mouseY, boolean isSelected, float partialTicks);

        /**
         * Called when the mouse is clicked within this entry. Returning true means that something within this entry was
         * clicked and the list should not be dragged.
         */
        boolean mousePressed(int slotIndex, int mouseX, int mouseY, int mouseEvent, int relativeX, int relativeY);

        /**
         * Fired when the mouse button is released. Arguments: index, x, y, mouseEvent, relativeX, relativeY
         */
        void mouseReleased(int slotIndex, int x, int y, int mouseEvent, int relativeX, int relativeY);

    }
}
