package com.lalameow.serverlogin.client.gui;

import com.lalameow.serverlogin.ServerLoginMod;
import com.lalameow.serverlogin.client.ClientProxy;
import com.lalameow.serverlogin.client.gui.element.GuiElement;
import com.lalameow.serverlogin.client.gui.element.GuiPasswordField;
import com.lalameow.serverlogin.client.gui.element.IGuiScreen;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/25
 * 时间: 23:20
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class GuiEditPsw extends IGuiScreen {
    private GuiScreen parent;
    private ClientProxy clientProxy;

    private int textColor = 0xe0e0e0;

    private int boxHeight;
    private int boxWidth;
    private int boxMargin;

    private int halfScreenWidth;
    private int halfScreenHeight;

    private String message = "";

    private GuiElement TEXT_FIELD_PASSWORD;
    private GuiElement TEXT_FIELD_NEW;
    private GuiElement TEXT_FIELD_CONFIRM;

    private GuiElement BUTTON_ACCEPT;
    private GuiElement BUTTON_BACK;
    private GuiElement TEXT_MESSAGE;

    private GuiPasswordField textFieldPassword;
    private GuiPasswordField textFieldNew;
    private GuiPasswordField textFieldConfirm;
    private String userName;
    private String userNameLable;
    private GuiButton btnAccept;
    GuiEditPsw(GuiScreen parent,String userName) {

        this.parent = parent;
        this.userName=userName;
        clientProxy=(ClientProxy) ServerLoginMod.proxy;
    }

    @Override
    public void initGui() {
        this.buttonList.clear();
        this.labelList.clear();
        this.textFieldList.clear();
        this.userNameLable=I18n.format("sl.edit.userlable");
        boxHeight = 20;
        boxWidth = 80;
        boxMargin = 4;

        halfScreenWidth = this.width / 2;
        halfScreenHeight = this.height / 2;

        TEXT_FIELD_PASSWORD = new GuiElement(
                0,
                "sl.password",
                new GuiElement.GuiRect(
                        halfScreenWidth - boxWidth - boxMargin / 2,
                        (int) (halfScreenHeight - boxMargin * 1.5 - boxHeight * 2),
                        boxWidth * 2 + boxMargin, boxHeight
                )
        );
        TEXT_FIELD_NEW = new GuiElement(
                1,
                "sl.edit.new",
                new GuiElement.GuiRect(
                        halfScreenWidth - boxWidth - boxMargin / 2,
                        (int) (halfScreenHeight - boxMargin * 0.5 - boxHeight),
                        boxWidth * 2 + boxMargin, boxHeight
                )
        );
        TEXT_FIELD_CONFIRM = new GuiElement(
                2,
                "sl.edit.confirm",
                new GuiElement.GuiRect(
                        halfScreenWidth - boxWidth - boxMargin / 2,
                        (int) (halfScreenHeight + boxMargin * 0.5),
                        boxWidth * 2 + boxMargin, boxHeight
                )
        );
        BUTTON_ACCEPT = new GuiElement(
                0,
                "gui.done",
                new GuiElement.GuiRect(
                        halfScreenWidth - boxWidth,
                        (int) (halfScreenHeight + boxMargin * 1.5 + boxHeight),
                        boxWidth, boxHeight
                )
        );
        BUTTON_BACK = new GuiElement(
                1,
                "gui.back",
                new GuiElement.GuiRect(
                        halfScreenWidth + boxMargin / 2,
                        (int) (halfScreenHeight + boxMargin * 1.5 + boxHeight),
                        boxWidth, boxHeight
                )
        );
        TEXT_MESSAGE = new GuiElement(
                new GuiElement.GuiRect(
                        -1, (int) (halfScreenHeight - boxMargin * 2.5 - boxHeight * 3)
                )
        );

        btnAccept=this.addButton(BUTTON_ACCEPT);
        this.addButton(BUTTON_BACK);

        this.textFieldPassword = addPasswordField(TEXT_FIELD_PASSWORD);
        this.textFieldNew = addPasswordField(TEXT_FIELD_NEW);
        this.textFieldConfirm = addPasswordField(TEXT_FIELD_CONFIRM);

        this.textFieldList.add(textFieldPassword);
        this.textFieldList.add(textFieldNew);
        this.textFieldList.add(textFieldConfirm);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();

        this.drawCenteredString(fontRenderer,this.userNameLable+this.userName,
                halfScreenWidth ,
                TEXT_MESSAGE.getRect().getY(), textColor);
        btnAccept.enabled=!(!textFieldConfirm.getPassword().equals(textFieldNew.getPassword()) ||
                textFieldPassword.getPassword().length()<6 ||
                textFieldNew.getPassword().length()<6
        );
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
        if (keyCode == 1) {
            this.mc.displayGuiScreen(parent);
        }
        if (keyCode == 15) {
            for(int i=0;i<this.textFieldList.size();i++){
                if(this.textFieldList.get(i).isFocused()){
                    int next=i+1;
                    if(next>=this.textFieldList.size()){
                        next=0;
                    }
                    this.textFieldList.get(next).setFocused(true);
                    this.textFieldList.get(i).setFocused(false);
                    return;
                }
            }
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        if (button.id == BUTTON_ACCEPT.getID()) {
            //TODO 验证原密码
            clientProxy.mcLoginClient.updatePwd(this.userName,textFieldPassword.getPassword(),textFieldNew.getPassword());
            this.mc.displayGuiScreen(new GuiLoading(this));
            return;
        }
        this.mc.displayGuiScreen(parent);
    }


}
