package com.lalameow.serverlogin.client.gui.element;

import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;

/**
 * Author: SettingDust.
 * Date: 2018/4/7.
 */
public class GuiNav extends Gui {
    public int height;

    public GuiNav(int height) {
        this.height = height;
    }

    public void drawNav() {
        drawRect(0, 0, 100, height, 0x64000000);
    }
}
