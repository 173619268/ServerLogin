package com.lalameow.serverlogin.client.gui.element;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;

/**
 * Author: SettingDust.
 * Date: 2018/4/14.
 */
public class GuiTextFieldWithTitle extends GuiTextField {
    private final FontRenderer fontRenderer;
    private String title;

    public GuiTextFieldWithTitle(int componentId, FontRenderer fontRenderer, int x, int y, int par5Width, int par6Height, String title) {
        super(componentId, fontRenderer, x, y, par5Width, par6Height);
        this.fontRenderer = fontRenderer;
        this.title = I18n.format(title);
    }

    @Override
    public void drawTextBox() {
        super.drawTextBox();
        this.drawString(fontRenderer, title,
                x - fontRenderer.getStringWidth(title) - 4,
                y + (height - fontRenderer.FONT_HEIGHT) / 2,
                0xe0e0e0);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
