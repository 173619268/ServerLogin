package com.lalameow.serverlogin.client.gui;

import com.lalameow.packet.pojo.ServerInfo;
import com.lalameow.serverlogin.client.ClientProxy;
import com.lalameow.serverlogin.client.gui.common.SelectServerList;
import com.lalameow.serverlogin.client.gui.common.ServerListElement;
import com.lalameow.serverlogin.client.server.IServerData;
import com.lalameow.serverlogin.third.AltManager;
import net.minecraft.client.gui.*;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.multiplayer.ServerList;
import net.minecraft.client.network.LanServerInfo;
import net.minecraft.client.network.ServerPinger;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

import java.io.IOException;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/25
 * 时间: 23:19
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class GuiSelectServer extends GuiScreen {

    private SelectServerList serverListSelector;
    private ServerList savedServerList;

    private GuiScreen parent;

    private String hoveringText;

    private final ServerPinger oldServerPinger = new ServerPinger();
    private ServerInfo[] serverInfos;
    private String playerName;
    public GuiSelectServer(GuiScreen parent, ServerInfo[] serverInfos,String playerName) {

        this.parent = parent;
        this.serverInfos=serverInfos;
        this.playerName=playerName;
    }

    @Override
    public void initGui() {

        Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();
        net.minecraftforge.fml.client.FMLClientHandler.instance().setupServerList();

        savedServerList = new ServerList(this.mc);
        this.clearServersList();
        this.serverListSelector = new SelectServerList(this.mc, this.width, this.height, 32, this.height - 64, 36, this);
        this.serverListSelector.clearOnlineServers();

        if(serverInfos!=null){
            for (ServerInfo serverInfo : serverInfos) {
                IServerData serverData = new IServerData(serverInfo.getServerName(), serverInfo.getServerHost(), false);
                savedServerList.addServerData(serverData);
                ServerData mcserverData=new ServerData(serverInfo.getServerName(), serverInfo.getServerHost(), false);
                FMLClientHandler.instance().bindServerListData(mcserverData,null);
            }

        }
        AltManager.getInstance().setUserOffline(this.playerName,UUID.randomUUID().toString() );
        this.serverListSelector.updateOnlineServers(savedServerList);
        if(playerName==null){
            this.mc.displayGuiScreen(new GuiMsg(new IGuiMainMenu(),I18n.format("sl.invaidplayer")));
        }

    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        this.serverListSelector.drawScreen(mouseX, mouseY, partialTicks);
        this.drawCenteredString(this.fontRenderer, I18n.format("sl.selectserver"), this.width / 2, 20, 16777215);
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        //ESC键位判断
        if (keyCode == 1) {
            this.mc.displayGuiScreen(parent);
        }
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        this.clearServersList();
    }

    private void clearServersList() {
        for (int i = 0; i < savedServerList.countServers(); i++) {
            savedServerList.removeServerData(i);
        }
        try {
            savedServerList.saveServerList();
        } catch (Exception e) {
        }

    }

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
        this.serverListSelector.handleMouseInput();
    }

    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.serverListSelector.mouseClicked(mouseX, mouseY, mouseButton);
    }

    protected void mouseReleased(int mouseX, int mouseY, int state) {
        super.mouseReleased(mouseX, mouseY, state);
        this.serverListSelector.mouseReleased(mouseX, mouseY, state);
    }

    public ServerPinger getOldServerPinger() {
        return this.oldServerPinger;
    }

    public void setHoveringText(String hoveringText) {
        this.hoveringText = hoveringText;
    }

    public boolean canMoveUp(ServerListElement p_175392_1_, int p_175392_2_) {
        return p_175392_2_ > 0;
    }

    public boolean canMoveDown(ServerListElement p_175394_1_, int p_175394_2_) {
        return p_175394_2_ < this.savedServerList.countServers() - 1;
    }

    public void selectServer(int index) {
        this.serverListSelector.setSelectedSlotIndex(index);
        GuiListExtended.IGuiListEntry iGuiListEntry = index < 0 ? null : this.serverListSelector.getListEntry(index);

        if (iGuiListEntry != null && !(iGuiListEntry instanceof ServerListEntryLanScan)) {

            if (iGuiListEntry instanceof ServerListElement) {
            }
        }
    }

    public void connectToSelected() {
        GuiListExtended.IGuiListEntry guilistextendedGuilistentry = this.serverListSelector.getSelected() < 0 ? null : this.serverListSelector.getListEntry(this.serverListSelector.getSelected());

        if (guilistextendedGuilistentry instanceof ServerListElement) {
            this.connectToServer(((ServerListElement) guilistextendedGuilistentry).getServerData());
        } else if (guilistextendedGuilistentry instanceof ServerListEntryLanDetected) {
            LanServerInfo lanserverinfo = ((ServerListEntryLanDetected) guilistextendedGuilistentry).getServerData();
            this.connectToServer(new ServerData(lanserverinfo.getServerMotd(), lanserverinfo.getServerIpPort(), true));
        }
    }

    private void connectToServer(ServerData server) {
//        net.minecraftforge.fml.client.FMLClientHandler.instance().connectToServer(parent, server);

        this.mc.displayGuiScreen(new GuiConnenctionServer(parent, this.mc,server));
    }

    public void moveServerUp(ServerListElement p_175391_1_, int p_175391_2_, boolean p_175391_3_) {
        int i = p_175391_3_ ? 0 : p_175391_2_ - 1;
        this.savedServerList.swapServers(p_175391_2_, i);

        if (this.serverListSelector.getSelected() == p_175391_2_) {
            this.selectServer(i);
        }

        this.serverListSelector.updateOnlineServers(this.savedServerList);
    }

    public void moveServerDown(ServerListElement p_175393_1_, int p_175393_2_, boolean p_175393_3_) {
        int i = p_175393_3_ ? this.savedServerList.countServers() - 1 : p_175393_2_ + 1;
        this.savedServerList.swapServers(p_175393_2_, i);

        if (this.serverListSelector.getSelected() == p_175393_2_) {
            this.selectServer(i);
        }

        this.serverListSelector.updateOnlineServers(this.savedServerList);
    }

}
