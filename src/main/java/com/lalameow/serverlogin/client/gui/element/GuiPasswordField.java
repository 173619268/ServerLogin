package com.lalameow.serverlogin.client.gui.element;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.StringUtils;

/**
 * Author: SettingDust.
 * Date: 2018/4/12.
 */
public class GuiPasswordField extends GuiTextFieldWithTitle {
    private String password = "";

    public GuiPasswordField(int componentId, FontRenderer fontRenderer, int x, int y, int par5Width, int par6Height, String title) {
        super(componentId, fontRenderer, x, y, par5Width, par6Height, title);
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public void setTextString(){
        if(this.password!=null){
            String mark="";
            for(int i=0;i<this.password.length();i++){
                 mark+="*";
            }
            this.setText(mark);
        }

    }
    @Override
    public boolean textboxKeyTyped(char typedChar, int keyCode) {
        boolean flag = super.textboxKeyTyped(typedChar, keyCode);
        if ((int) typedChar != 0 && !GuiScreen.isCtrlKeyDown()) {
            if (!StringUtils.isNullOrEmpty(this.getText()) || !StringUtils.isNullOrEmpty(password)) {
                if (typedChar != 8) {
                    password += String.valueOf(typedChar);
                    this.setText(this.getText().replace(String.valueOf(typedChar), "*"));
                } else if (this.getText().length() >= 0) {
                    password = password.substring(0, password.length() - 1);
                }
                password = password.replace("null", "");
            }
        }
        return flag;
    }
}
