package com.lalameow.serverlogin.client.gui.common;

import com.google.common.collect.Lists;
import com.lalameow.serverlogin.client.gui.GuiSelectServer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiListExtended;
import net.minecraft.client.gui.ServerListEntryLanDetected;
import net.minecraft.client.gui.ServerListEntryLanScan;
import net.minecraft.client.multiplayer.ServerList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/26
 * 时间: 0:45
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class SelectServerList extends GuiListExtended {
    private final List<ServerListElement> serverListInternet = Lists.newArrayList();
    private final List<ServerListEntryLanDetected> serverListLan = Lists.newArrayList();
    private final IGuiListEntry lanScanEntry = new ServerListEntryLanScan();
    private int selectedSlotIndex = -1;
    private GuiSelectServer owner;

    public SelectServerList(Minecraft mcIn, int width, int height, int topIn, int bottomIn, int slotHeightIn, GuiSelectServer owner) {
        super(mcIn, width, height, topIn, bottomIn, slotHeightIn);
        this.owner = owner;
    }

    @Override
    public IGuiListEntry getListEntry(int i) {
        if (i < this.serverListInternet.size()) {
            return (IGuiListEntry) this.serverListInternet.get(i);
        } else {
            i -= this.serverListInternet.size();
            if (i == 0) {
                return this.lanScanEntry;
            } else {
                --i;
                return (IGuiListEntry) this.serverListLan.get(i);
            }
        }
    }

    @Override
    protected int getSize() {
        return this.serverListInternet.size() + 1 + this.serverListLan.size();
    }

    public void setSelectedSlotIndex(int p_setSelectedSlotIndex_1_) {
        this.selectedSlotIndex = p_setSelectedSlotIndex_1_;
    }

    protected boolean isSelected(int p_isSelected_1_) {
        return p_isSelected_1_ == this.selectedSlotIndex;
    }

    public int getSelected() {
        return this.selectedSlotIndex;
    }

    public void updateOnlineServers(ServerList updateOnlineServers) {
        this.serverListInternet.clear();
        for (int i = 0; i < updateOnlineServers.countServers(); i++) {
            this.serverListInternet.add(new ServerListElement(this.owner, updateOnlineServers.getServerData(i)));
        }

    }

    public void clearOnlineServers() {
        this.serverListInternet.clear();
    }
//    public void updateNetworkServers(List<LanServerInfo> updateNetworkServer) {
//        this.serverListLan.clear();
//        Iterator var2 = updateNetworkServer.iterator();
//
//        while(var2.hasNext()) {
//            LanServerInfo lanServerInfo = (LanServerInfo)var2.next();
//            this.serverListLan.add(new ServerListEntryLanDetected(this.owner, lanServerInfo));
//        }
//
//    }

    protected int getScrollBarX() {
        return super.getScrollBarX() + 30;
    }

    public int getListWidth() {
        return super.getListWidth() + 85;
    }
}
