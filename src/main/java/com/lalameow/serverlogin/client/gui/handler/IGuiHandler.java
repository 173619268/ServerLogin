package com.lalameow.serverlogin.client.gui.handler;

import com.lalameow.serverlogin.client.gui.GuiLogin;
import com.lalameow.serverlogin.client.gui.IGuiMainMenu;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/25
 * 时间: 3:27
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class IGuiHandler {
    @SubscribeEvent
    public void displayMainMenu(GuiOpenEvent event) throws IOException {
        if (event.getGui() != null
                && event.getGui() instanceof GuiMainMenu) {
            event.setGui(new IGuiMainMenu());
        }
        if (event.getGui() != null) {
            if (GuiMultiplayer.class.equals(event.getGui().getClass())) {
                event.setGui(new GuiLogin(new IGuiMainMenu()));
            }
        }
    }
}
