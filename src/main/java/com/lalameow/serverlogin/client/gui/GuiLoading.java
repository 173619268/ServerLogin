package com.lalameow.serverlogin.client.gui;

import com.lalameow.packet.BasePack;
import com.lalameow.packet.packImpl.EditPswRecivePack;
import com.lalameow.packet.packImpl.LoginRecivePack;
import com.lalameow.packet.packImpl.PacketAbstract;
import com.lalameow.packet.packImpl.RegistRecivePack;
import com.lalameow.serverlogin.ServerLoginMod;
import com.lalameow.serverlogin.client.ClientProxy;
import com.lalameow.serverlogin.client.gui.element.GuiElement;
import com.lalameow.serverlogin.client.gui.element.IGuiScreen;
import com.lalameow.serverlogin.network.ClientState;
import com.lalameow.serverlogin.network.McLoginClient;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.IOException;
import java.security.PublicKey;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/25
 * 时间: 23:19
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class GuiLoading extends IGuiScreen {

    private ClientProxy clientProxy;
    private String loadingstr;
    private String timeoutstr;

    private GuiScreen parent;
    private int halfScreenWidth;
    private int halfScreenHeight;
    private int douhaoCount = -1;
    private String douhao = "";
    private long startTime = 0;

    private int boxMargin;
    private int boxHeight;
    private int textColor;
    private GuiElement BUTTON_BACK;

    public GuiLoading(GuiScreen parent) {
        this.parent = parent;
        clientProxy = (ClientProxy) ServerLoginMod.proxy;
    }


    @Override
    public void initGui() {
        this.buttonList.clear();
        loadingstr = I18n.format("sl.loading");
        timeoutstr = I18n.format("sl.timeout");

        startTime = System.currentTimeMillis();
        halfScreenWidth = this.width / 2;
        halfScreenHeight = this.height / 2;
        this.buttonList.clear();
        //General
        int boxWidth = 80;
        boxHeight = 20;
        boxMargin = 4;

        textColor = 0xe0e0e0;

        BUTTON_BACK = new GuiElement(3, "gui.back",
                new GuiElement.GuiRect(
                        halfScreenWidth - boxWidth / 2 - 5,
                        (int) (halfScreenHeight + boxMargin * 2.5 + boxHeight * 2.5),
                        boxWidth, boxHeight
                ));
        this.addButton(BUTTON_BACK);
    }


    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.drawDefaultBackground();
        long timeout = (System.currentTimeMillis() - startTime) / 1000;
        if (timeout < 30) {
            drawLoading();
            checkConnection();
        } else {
            drawTimeout();
            McLoginClient.clientState = ClientState.EXCEPTION;
        }
        super.drawScreen(par1, par2, par3);
    }

    /**
     * 处理各返回值
     */
    private void checkConnection() {
        BasePack basePack = clientProxy.packQueue.poll();
        if (basePack != null) {
            PacketAbstract packetAbstract = basePack.getPackType().getObject(basePack.getBodyContent());
            if (packetAbstract instanceof LoginRecivePack) {
                LoginRecivePack loginRecivePack = (LoginRecivePack) packetAbstract;
                if (loginRecivePack.isLogin()) {
                    ClientProxy.publicKey=(PublicKey)basePack.getKey();
                    ClientProxy.clientId=basePack.getClientId();
                    this.mc.displayGuiScreen(new GuiSelectServer(this.parent, loginRecivePack.getServerInfoArray(), loginRecivePack.getPlayerName()));
                } else {
                    this.mc.displayGuiScreen(new GuiMsg(this.parent, loginRecivePack.getMsg()));
                }
            } else if (packetAbstract instanceof RegistRecivePack) {
                RegistRecivePack registRecivePack = (RegistRecivePack) packetAbstract;
                if (registRecivePack.isRegistOk()) {
                    this.mc.displayGuiScreen(new GuiMsg(new GuiLogin(new IGuiMainMenu()), registRecivePack.getMsg()));
                } else {
                    this.mc.displayGuiScreen(new GuiMsg(this.parent, registRecivePack.getMsg()));
                }
            } else if (packetAbstract instanceof EditPswRecivePack) {
                EditPswRecivePack editPswRecivePack = (EditPswRecivePack) packetAbstract;
                if (editPswRecivePack.isEditOk()) {
                    this.mc.displayGuiScreen(new GuiMsg(new GuiLogin(new IGuiMainMenu()), editPswRecivePack.getMsg()));
                } else {
                    this.mc.displayGuiScreen(new GuiMsg(this.parent, editPswRecivePack.getMsg()));
                }
            }
        }
    }

    /**
     * 连接超时
     */
    private void drawTimeout() {
        this.drawCenteredString(
                this.fontRenderer,
                timeoutstr,
                halfScreenWidth,
                halfScreenHeight - 40,
                0xe0e0e0
        );
    }

    /**
     * 渲染加载效果
     */
    private void drawLoading() {
        if (douhaoCount % 20 == 0) {
            douhao += ".";
        }
        if (douhaoCount > (100)) {
            douhaoCount = 0;
            douhao = "";
        }
        douhaoCount++;
        this.drawCenteredString(
                this.fontRenderer,
                loadingstr + douhao,
                halfScreenWidth,
                halfScreenHeight - 40,
                0xe0e0e0
        );
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) throws IOException {
        if (guiButton.id == BUTTON_BACK.getID()) {
            this.mc.displayGuiScreen(this.parent);
        }
    }
}
