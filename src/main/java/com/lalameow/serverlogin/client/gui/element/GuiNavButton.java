package com.lalameow.serverlogin.client.gui.element;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;


/**
 * Author: SettingDust.
 * Date: 2018/4/7.
 */
public class GuiNavButton extends GuiButton {
    public GuiNavButton(int buttonId, String buttonText) {
        super(buttonId, 0, buttonId * 20 + 68, 100, 20, buttonText);
        this.width = 100;
        this.height = 20;
        this.enabled = true;
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        FontRenderer fontrenderer = mc.fontRenderer;
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
        if (hovered)
            drawRect(x, y, x + width, y + height, 0x55ffffff);
        this.mouseDragged(mc, mouseX, mouseY);
        int j = 0xe8e8e8;

        if (!this.enabled) {
            j = 0xa0a0a0;
        } else if (this.hovered) {
            j = 0xffffa0;
        }

        this.drawCenteredString(fontrenderer, this.displayString, this.x + this.width / 2, this.y + (this.height - 8) / 2, j);
    }

    @Override
    public void playPressSound(SoundHandler soundHandlerIn) {

    }
}
