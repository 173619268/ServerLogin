package com.lalameow.serverlogin.client.gui;

import com.lalameow.packet.packImpl.LoginPack;
import com.lalameow.serverlogin.ServerLoginMod;
import com.lalameow.serverlogin.client.ClientProxy;
import com.lalameow.serverlogin.client.config.ConfigManager;
import com.lalameow.serverlogin.client.gui.element.GuiElement;
import com.lalameow.serverlogin.client.gui.element.GuiElement.GuiRect;
import com.lalameow.serverlogin.client.gui.element.IGuiScreen;
import com.lalameow.serverlogin.client.gui.element.GuiTextFieldWithTitle;
import com.lalameow.serverlogin.client.gui.element.GuiPasswordField;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.StringUtils;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/25
 * 时间: 3:34
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class GuiLogin extends IGuiScreen {

    private int textColor;
    private ClientProxy clientProxy;
    private int boxHeight;
    private int boxMargin;
    /**
     * Button
     */
    private GuiElement BUTTON_LOGIN;
    private GuiElement BUTTON_REG;
    private GuiElement BUTTON_BACK;
    private GuiElement BUTTON_CHANGE;

    private GuiButton btnLogin;
    private GuiButton btnReg;
    private GuiButton btnBack;
    private GuiButton btnChange;

    /**
     * Username
     */
    private GuiElement TEXT_FIELD_USERNAME;
    private GuiTextFieldWithTitle textUserName;
    /**
     * Password
     */
    private GuiElement TEXT_FIELD_PASSWORD;
    private GuiPasswordField textPassword;

    private GuiScreen parent;
    /**
     * Data
     */
    private String userName = null;
    private static String password = "";

    public GuiLogin(GuiScreen parent) {
        this.parent = parent;
        clientProxy = (ClientProxy) ServerLoginMod.proxy;
    }


    @Override
    public void initGui() {
        Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();
        this.labelList.clear();
        this.textFieldList.clear();
        //General
        int boxWidth = 80;
        boxHeight = 20;
        boxMargin = 4;

        textColor = 0xe0e0e0;

        /*
          Sizes
        */
        int halfScreenWidth = this.width / 2;
        int halfScreenHeight = this.height / 2;

        //Buttons
        {
            BUTTON_LOGIN = new GuiElement(0, "sl.login",
                    new GuiRect(
                            halfScreenWidth - boxWidth,
                            halfScreenHeight + boxMargin / 2 - boxHeight / 2,
                            boxWidth * 2 + boxMargin, boxHeight
                    ));
            BUTTON_CHANGE = new GuiElement(1, "sl.editpws",
                    new GuiRect(
                            halfScreenWidth - boxWidth,
                            halfScreenHeight + (int) (boxMargin * 1.5) + boxHeight / 2,
                            boxWidth * 2 + boxMargin, boxHeight
                    ));
            BUTTON_REG = new GuiElement(2, "sl.login.register",
                    new GuiRect(
                            halfScreenWidth - boxWidth,
                            (int) (halfScreenHeight + boxMargin * 2.5 + boxHeight * 1.5),
                            boxWidth, boxHeight
                    ));
            BUTTON_BACK = new GuiElement(3, "gui.back",
                    new GuiRect(
                            halfScreenWidth + boxMargin,
                            (int) (halfScreenHeight + boxMargin * 2.5 + boxHeight * 1.5),
                            boxWidth, boxHeight
                    ));

            btnLogin = this.addButton(BUTTON_LOGIN);
            btnReg = this.addButton(BUTTON_REG);
            btnBack = this.addButton(BUTTON_BACK);
            btnChange = this.addButton(BUTTON_CHANGE);
        }
        //Labels & TextFiled
        {
            //Username
            {

                TEXT_FIELD_USERNAME = new GuiElement("sl.username",
                        new GuiRect(
                                halfScreenWidth - boxWidth,
                                (int) (halfScreenHeight - boxHeight * 2.5 - boxMargin * 1.5),
                                boxWidth * 2 + boxMargin, boxHeight
                        ));

                textUserName = new GuiTextFieldWithTitle(
                        TEXT_FIELD_USERNAME.getID(), this.fontRenderer,
                        TEXT_FIELD_USERNAME.getRect().getX(),
                        TEXT_FIELD_USERNAME.getRect().getY(),
                        TEXT_FIELD_USERNAME.getRect().getWidth(),
                        TEXT_FIELD_USERNAME.getRect().getHeight(),
                        TEXT_FIELD_USERNAME.getText());
                textUserName.setText(ConfigManager.username);
                textUserName.setMaxStringLength(20);
                this.textFieldList.add(textUserName);
            }
            //Password
            {
                TEXT_FIELD_PASSWORD = new GuiElement("sl.password",
                        new GuiRect(
                                halfScreenWidth - boxWidth,
                                (int) (halfScreenHeight - boxHeight * 1.5 - boxMargin / 2),
                                boxWidth * 2 + boxMargin, boxHeight
                        ));

                textPassword = new GuiPasswordField(TEXT_FIELD_PASSWORD.getID(),
                        this.fontRenderer,
                        TEXT_FIELD_PASSWORD.getRect().getX(),
                        TEXT_FIELD_PASSWORD.getRect().getY(),
                        TEXT_FIELD_PASSWORD.getRect().getWidth(),
                        TEXT_FIELD_PASSWORD.getRect().getHeight(),
                        TEXT_FIELD_PASSWORD.getText()
                );
                textPassword.setMaxStringLength(18);
                textPassword.setPassword(password);
                textPassword.setTextString();
                this.textFieldList.add(textPassword);
            }
        }
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.drawDefaultBackground();
        btnLogin.enabled = !(textPassword.getPassword().length() < 6 || StringUtils.isNullOrEmpty(textUserName.getText()));
        super.drawScreen(par1, par2, par3);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) throws IOException {
        if (guiButton.id == BUTTON_BACK.getID()) {
            this.mc.displayGuiScreen(parent);
        } else if (guiButton.id == BUTTON_REG.getID()) {
            this.mc.displayGuiScreen(new GuiRegister(this));
        } else if (guiButton.id == BUTTON_LOGIN.getID()) {
            this.login();
        } else if (guiButton.id == BUTTON_CHANGE.getID()) {
            //这里修改密码时候将用户名保存一下，用在回调
            ConfigManager.username = this.textUserName.getText();
            ConfigManager.save();
            this.mc.displayGuiScreen(new GuiEditPsw(this, textUserName.getText()));
        }
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
        if (!Character.isLetterOrDigit(typedChar)
                && !String.valueOf(typedChar).equalsIgnoreCase("_")) {
            textUserName.setText(textUserName.getText().replace(String.valueOf(typedChar), ""));
        }
        if (keyCode == 15) {
            if (this.textUserName.isFocused()) {
                this.textPassword.setFocused(true);
                this.textUserName.setFocused(false);
            } else if (this.textPassword.isFocused()) {
                this.textUserName.setFocused(true);
                this.textPassword.setFocused(false);
            }
        }
        if (keyCode == 28) {
            if (this.textPassword.getPassword().length() >= 6 && this.textUserName.getText().length() > 0) {
                this.login();
            }
        }

        if (keyCode == 1) {
            this.mc.displayGuiScreen(parent);
        }
    }

    private void login() {
        this.userName = textUserName.getText();
        this.password = textPassword.getPassword();
        //TODO Login
        ConfigManager.username = userName;
        ConfigManager.save();

        clientProxy.mcLoginClient.login(userName, password);
        this.mc.displayGuiScreen(new GuiLoading(this));
    }
}
