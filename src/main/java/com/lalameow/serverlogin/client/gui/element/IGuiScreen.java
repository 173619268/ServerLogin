package com.lalameow.serverlogin.client.gui.element;

import com.google.common.collect.Lists;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

import java.io.IOException;
import java.util.List;

/**
 * Author: SettingDust.
 * Date: 2018/4/12.
 */
public class IGuiScreen extends GuiScreen {
    protected List<GuiTextField> textFieldList = Lists.newArrayList();

    protected GuiButton addButton(GuiElement element) {
        return super.addButton(
                new GuiButton(
                        element.getID(),
                        element.getRect().getX(),
                        element.getRect().getY(),
                        element.getRect().getWidth(),
                        element.getRect().getHeight(),
                        element.getText()
                )
        );
    }

    //todo 你这方法有毒
//    protected GuiTextFieldWithTitle addTextField(GuiElement element) {
//        GuiTextFieldWithTitle textFieldWithTitle = new GuiTextFieldWithTitle(
//                element.getID(), fontRenderer,
//                element.getRect().getX(),
//                element.getRect().getY(),
//                element.getRect().getWidth(),
//                element.getRect().getHeight(),
//                element.getText());
//        textFieldList.add(textFieldWithTitle);
//        return textFieldWithTitle;
//    }

    protected GuiPasswordField addPasswordField(GuiElement element) {
        GuiPasswordField passwordField = new GuiPasswordField(
                element.getID(), fontRenderer,
                element.getRect().getX(),
                element.getRect().getY(),
                element.getRect().getWidth(),
                element.getRect().getHeight(),
                element.getText());
        return passwordField;
    }

    @Override
    protected void mouseClicked(int p_mouseClicked_1_, int p_mouseClicked_2_, int p_mouseClicked_3_) throws IOException {
        super.mouseClicked(p_mouseClicked_1_, p_mouseClicked_2_, p_mouseClicked_3_);
        for (GuiTextField guiTextField : textFieldList) {
            guiTextField.mouseClicked(p_mouseClicked_1_, p_mouseClicked_2_, p_mouseClicked_3_);
        }
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        for (GuiTextField guiTextField : textFieldList) {
            if (guiTextField.isFocused()) {
                super.keyTyped(typedChar, keyCode);
                guiTextField.textboxKeyTyped(typedChar, keyCode);
            }
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
        for (GuiTextField guiTextField : textFieldList) {
            guiTextField.drawTextBox();
        }
    }
}
