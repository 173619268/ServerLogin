package com.lalameow.serverlogin.client.gui;

import com.lalameow.packet.packImpl.RegistPack;
import com.lalameow.serverlogin.ServerLoginMod;
import com.lalameow.serverlogin.client.ClientProxy;
import com.lalameow.serverlogin.client.config.ConfigManager;
import com.lalameow.serverlogin.client.gui.element.GuiPasswordField;
import com.lalameow.serverlogin.client.gui.element.GuiTextFieldWithTitle;
import com.lalameow.serverlogin.client.gui.element.IGuiScreen;
import com.lalameow.serverlogin.util.AuthorizationUtils;
import com.lalameow.serverlogin.util.DateUtil;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.StringUtils;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/25
 * 时间: 23:19
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class GuiRegister extends IGuiScreen {
    private ClientProxy clientProxy;
    private final int BTNREGIST = 0, BTNBACK = 1;
    private int halfScreenWidth = 0;
    private int halfScreenHeight = 0;
    private int elementWidth = 160;
    private int elementHeight = 20;
    private int firstElemX = 0;
    private int firstElemY = 0;

    private GuiButton btnRegist;
    private GuiButton btnBack;

    private GuiScreen parent;

    private GuiTextFieldWithTitle textPlayerName;
    private GuiTextFieldWithTitle textUserName;
    private GuiPasswordField textpassword;
    private GuiPasswordField textAginPassword;
    private GuiTextFieldWithTitle textQQ;
    private GuiTextFieldWithTitle textTuijian;
    private GuiTextFieldWithTitle textYaoqingMa;

    public GuiRegister(GuiScreen parent) {
        this.parent = parent;
        clientProxy = (ClientProxy) ServerLoginMod.proxy;
    }

    @Override
    public void initGui() {
        Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();
        this.labelList.clear();
        this.textFieldList.clear();
        halfScreenWidth = this.width / 2;
        halfScreenHeight = this.height / 2;
        firstElemX = halfScreenWidth - 80;
        firstElemY = halfScreenHeight - 90;

        //TODO GuiElement

        textPlayerName = new GuiTextFieldWithTitle(0, this.fontRenderer, firstElemX, firstElemY, elementWidth, elementHeight, I18n.format("sl.playername"));
        textUserName = new GuiTextFieldWithTitle(1, this.fontRenderer, firstElemX, firstElemY + elementHeight * 1 + 5 * 1, elementWidth, elementHeight, I18n.format("sl.username"));
        textpassword = new GuiPasswordField(2, this.fontRenderer, firstElemX, firstElemY + elementHeight * 2 + 5 * 2, elementWidth, elementHeight, I18n.format("sl.password"));
        textAginPassword = new GuiPasswordField(3, this.fontRenderer, firstElemX, firstElemY + elementHeight * 3 + 5 * 3, elementWidth, elementHeight, I18n.format("sl.apassword"));
        textQQ = new GuiTextFieldWithTitle(5, this.fontRenderer, firstElemX, firstElemY + elementHeight * 4 + 5 * 4, elementWidth, elementHeight, I18n.format("sl.qq"));
        textTuijian = new GuiTextFieldWithTitle(6, this.fontRenderer, firstElemX, firstElemY + elementHeight * 5 + 5 * 5, elementWidth, elementHeight, I18n.format("sl.tuijianren"));
        textYaoqingMa = new GuiTextFieldWithTitle(7, this.fontRenderer, firstElemX, firstElemY + elementHeight * 6 + 5 * 6, elementWidth, elementHeight, I18n.format("sl.yaoqingma"));

        textFieldList.add(textPlayerName);
        textFieldList.add(textUserName);
        textFieldList.add(textpassword);
        textFieldList.add(textAginPassword);
        textFieldList.add(textQQ);
        textFieldList.add(textTuijian);
        textFieldList.add(textYaoqingMa);

        this.btnRegist = new GuiButton(BTNREGIST, firstElemX, firstElemY + elementHeight * 7 + 5 * 8, 70, elementHeight, I18n.format("sl.register"));
        this.btnBack = new GuiButton(BTNBACK, firstElemX + 75, firstElemY + elementHeight * 7 + 5 * 8, 70, elementHeight, I18n.format("gui.back"));
        this.buttonList.add(btnRegist);
        this.buttonList.add(btnBack);
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.drawDefaultBackground();
        this.fontRenderer.drawString(I18n.format("sl.newregesit"), this.halfScreenWidth - 25, firstElemY - 20, 52224);

        this.btnRegist.drawButton(this.mc, par1, par2, par3);
        this.btnBack.drawButton(this.mc, par1, par2, par3);

        this.drawReginfo();
        super.drawScreen(par1, par2, par3);
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
        if (keyCode == 1) {
            this.mc.displayGuiScreen(parent);
        }
        if (keyCode == 15) {
            for (int i = 0; i < this.textFieldList.size(); i++) {
                if (this.textFieldList.get(i).isFocused()) {
                    int next = i + 1;
                    if (next >= this.textFieldList.size()) {
                        next = 0;
                    }
                    this.textFieldList.get(i).setFocused(false);
                    this.textFieldList.get(next).setFocused(true);
                    return;
                }
            }
        }
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) throws IOException {
        switch (guiButton.id) {
            case BTNREGIST:
                //TODO Register
                this.regist();
                break;
            case BTNBACK:
                this.mc.displayGuiScreen(parent);
                break;
        }
    }

    private final Pattern userNamePattern = Pattern.compile("^[a-zA-Z][a-zA-Z0-9_]*$");
    private final Pattern playerName = Pattern.compile("^[a-zA-Z0-9_]*$");

    /**
     * 绘制页面显示的字
     */
    private void drawReginfo() {
        int msgStrX = firstElemX + 165;
        int strY = firstElemY + 5;
        this.fontRenderer.drawString(I18n.format("sl.msgplayername"), msgStrX, strY, 16724736);
        this.fontRenderer.drawString(I18n.format("sl.msgusername"), msgStrX, strY + elementHeight * 1 + 5 * 1, 16724736);
        this.fontRenderer.drawString(I18n.format("sl.msgpassword"), msgStrX, strY + elementHeight * 2 + 5 * 2, 16724736);
        this.fontRenderer.drawString(I18n.format("sl.msgapassword"), msgStrX, strY + elementHeight * 3 + 5 * 3, 16724736);
        this.fontRenderer.drawString(I18n.format("sl.msgqq"), msgStrX, strY + elementHeight * 4 + 5 * 4, 16724736);
        this.fontRenderer.drawString(I18n.format("sl.msgtuijianren"), msgStrX, strY + elementHeight * 5 + 5 * 5, 16724736);
        this.fontRenderer.drawString(I18n.format("sl.msgyaoqingma"), msgStrX, strY + elementHeight * 6 + 5 * 6, 16724736);

        btnRegist.enabled = !(textpassword.getPassword().length() < 6 ||
                StringUtils.isNullOrEmpty(textUserName.getText()) ||
                StringUtils.isNullOrEmpty(textPlayerName.getText()) ||
                !textpassword.getPassword().equals(textAginPassword.getPassword()) ||
                textPlayerName.getText().length() < 3 ||
                textUserName.getText().length() < 6 ||
                !userNamePattern.matcher(textUserName.getText()).matches() ||
                !playerName.matcher(textPlayerName.getText()).matches()

        );
    }

    private void regist() {
        RegistPack registPack = new RegistPack();
        registPack.setPlayerName(textPlayerName.getText());
        registPack.setUserName(textUserName.getText());
        registPack.setPassword(textpassword.getPassword());
        registPack.setQq(textQQ.getText());
        registPack.setTuijianren(textTuijian.getText());
        registPack.setCode(textYaoqingMa.getText());
        registPack.setMac(AuthorizationUtils.getMac());
        registPack.setMachineCode(AuthorizationUtils.getMachineCode());
        registPack.setTimespan(System.currentTimeMillis());
        registPack.setRegistDate(DateUtil.getNowDateString());
        clientProxy.mcLoginClient.regist(registPack);
        ConfigManager.username = textUserName.getText();
        ConfigManager.save();
        this.mc.displayGuiScreen(new GuiLoading(this));
    }
}
