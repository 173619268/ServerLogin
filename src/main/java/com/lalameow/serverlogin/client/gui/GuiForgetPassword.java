package com.lalameow.serverlogin.client.gui;

import com.lalameow.serverlogin.client.gui.element.GuiElement;
import com.lalameow.serverlogin.client.gui.element.GuiPasswordField;
import com.lalameow.serverlogin.client.gui.element.IGuiScreen;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.IOException;

/**
 * Author: SettingDust.
 * Date: 2018/4/21.
 */
@SideOnly(Side.CLIENT)
public class GuiForgetPassword extends IGuiScreen {
    private GuiScreen parent;

    private int textColor = 0xe0e0e0;

    private int boxHeight;
    private int boxWidth;
    private int boxMargin;

    private int halfScreenWidth;
    private int halfScreenHeight;

    private String message = "";

    private GuiElement TEXT_FIELD_ACCOUNT;
    private GuiElement TEXT_FIELD_NEW;
    private GuiElement TEXT_FIELD_CONFIRM;

    private GuiElement BUTTON_ACCEPT;
    private GuiElement BUTTON_BACK;
    private GuiElement TEXT_MESSAGE;

    private GuiPasswordField textFieldAccount;
    private GuiPasswordField textFieldNew;
    private GuiPasswordField textFieldConfirm;

    GuiForgetPassword(GuiScreen parent) {
        this.parent = parent;
    }

    @Override
    public void initGui() {

        boxHeight = 20;
        boxWidth = 80;
        boxMargin = 4;

        halfScreenWidth = this.width / 2;
        halfScreenHeight = this.height / 2;

        TEXT_FIELD_ACCOUNT = new GuiElement(
                0,
                "sl.username",
                new GuiElement.GuiRect(
                        halfScreenWidth - boxWidth - boxMargin / 2,
                        (int) (halfScreenHeight - boxMargin * 1.5 - boxHeight * 2),
                        boxWidth * 2 + boxMargin, boxHeight
                )
        );
        TEXT_FIELD_NEW = new GuiElement(
                1,
                "sl.new",
                new GuiElement.GuiRect(
                        halfScreenWidth - boxWidth - boxMargin / 2,
                        (int) (halfScreenHeight - boxMargin * 0.5 - boxHeight),
                        boxWidth * 2 + boxMargin, boxHeight
                )
        );
        TEXT_FIELD_CONFIRM = new GuiElement(
                2,
                "sl.confirm",
                new GuiElement.GuiRect(
                        halfScreenWidth - boxWidth - boxMargin / 2,
                        (int) (halfScreenHeight + boxMargin * 0.5),
                        boxWidth * 2 + boxMargin, boxHeight
                )
        );
        BUTTON_ACCEPT = new GuiElement(
                0,
                "gui.done",
                new GuiElement.GuiRect(
                        halfScreenWidth - boxWidth - boxMargin / 2,
                        (int) (halfScreenHeight + boxMargin * 1.5 + boxHeight),
                        boxWidth, boxHeight
                )
        );
        BUTTON_BACK = new GuiElement(
                1,
                "gui.back",
                new GuiElement.GuiRect(
                        halfScreenWidth + boxMargin / 2,
                        (int) (halfScreenHeight + boxMargin * 1.5 + boxHeight),
                        boxWidth, boxHeight
                )
        );
        TEXT_MESSAGE = new GuiElement(
                new GuiElement.GuiRect(
                        -1, (int) (halfScreenHeight - boxMargin * 2.5 - boxHeight * 3)
                )
        );

        this.addButton(BUTTON_ACCEPT);
        this.addButton(BUTTON_BACK);

        this.textFieldAccount = addPasswordField(TEXT_FIELD_ACCOUNT);
        this.textFieldConfirm = addPasswordField(TEXT_FIELD_CONFIRM);
        this.textFieldNew = addPasswordField(TEXT_FIELD_NEW);
    }


    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        this.drawString(fontRenderer, message,
                halfScreenWidth - fontRenderer.getStringWidth(message) / 2,
                TEXT_MESSAGE.getRect().getY(), textColor);
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
        if (keyCode == 1) {
            this.mc.displayGuiScreen(parent);
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        if (button.id == BUTTON_ACCEPT.getID()) {
            //TODO 验证
            if (textFieldConfirm.getPassword().equals(textFieldNew.getPassword())) {
                //TODO 通信
            } else {
                message = I18n.format("sl.unequal");
                return;
            }
        }
        this.mc.displayGuiScreen(parent);
    }
}
