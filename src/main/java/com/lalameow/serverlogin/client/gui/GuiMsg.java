package com.lalameow.serverlogin.client.gui;

import com.lalameow.serverlogin.ServerLoginMod;
import com.lalameow.serverlogin.client.ClientProxy;
import com.lalameow.serverlogin.client.gui.element.GuiElement;
import com.lalameow.serverlogin.client.gui.element.IGuiScreen;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/8
 * 时间: 3:01
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class GuiMsg extends IGuiScreen {

    private GuiScreen parent;
    private int halfScreenWidth;
    private int halfScreenHeight;

    private int boxMargin;
    private int boxHeight;
    private int textColor;
    private boolean isError=false;
    private GuiElement BUTTON_BACK;
    private String errormsg;
    public GuiMsg(GuiScreen parent, String errormsg) {
        this.parent = parent;
        this.errormsg=errormsg;
    }

    @Override
    public void initGui() {
        this.buttonList.clear();
        halfScreenWidth = this.width / 2;
        halfScreenHeight= this.height / 2;
        this.buttonList.clear();
        //General
        int boxWidth = 80;
        boxHeight = 20;
        boxMargin = 4;

        textColor = 0xe0e0e0;

        BUTTON_BACK = new GuiElement(3, "gui.back",
                new GuiElement.GuiRect(
                        halfScreenWidth - boxWidth/2-5,
                        (int) (halfScreenHeight + boxMargin * 2.5 + boxHeight * 2.5),
                        boxWidth, boxHeight
                ));
        this.addButton(BUTTON_BACK);
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.drawDefaultBackground();
        this.drawCenteredString(
                this.fontRenderer,
                errormsg,
                halfScreenWidth,
                halfScreenHeight-40,
                0xe0e0e0
        );
        super.drawScreen(par1, par2, par3);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) throws IOException {
        if (guiButton.id == BUTTON_BACK.getID()) {
            this.mc.displayGuiScreen(this.parent);
        }
    }

}
