package com.lalameow.serverlogin.client.gui.element;

import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Author: SettingDust.
 * Date: 2018/4/10.
 */
@SideOnly(Side.CLIENT)
public class GuiElement {
    private int ID;
    private String text;
    private GuiRect rect;

    public GuiElement(int ID, String text, GuiRect rect) {
        this.ID = ID;
        this.text = text;
        this.rect = rect;
    }

    public GuiElement(GuiRect rect) {
        this.rect = rect;
    }

    public GuiElement(int ID, GuiRect rect) {
        this.ID = ID;
        this.rect = rect;
    }

    public GuiElement(int ID, String text) {
        this.ID = ID;
        this.text = text;
    }

    public GuiElement(String text, GuiRect rect) {
        this.text = text;
        this.rect = rect;
    }

    public GuiRect getRect() {
        return rect;
    }

    public String getText() {
        return I18n.format(text);
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public static class GuiRect {
        private int x;
        private int y;
        private int width;
        private int height;

        public GuiRect(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public GuiRect(int x, int y, int width, int height) {

            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }
    }
}
