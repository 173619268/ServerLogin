package com.lalameow.serverlogin.third;

import com.mojang.authlib.Agent;
import com.mojang.authlib.AuthenticationService;
import com.mojang.authlib.UserAuthentication;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import net.minecraft.client.Minecraft;
import net.minecraft.util.Session;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.UUID;
/**
 * @author mrebhan
 */
@SideOnly(Side.CLIENT)
public class AltManager {
	private static AltManager manager = null;
	private final UserAuthentication auth;
	//private String currentUser;
	//private String currentPass;

	private AltManager()  {
		UUID uuid = UUID.randomUUID();
		AuthenticationService authService = new LaLaMeowAuthenticationService();
		auth = authService.createUserAuthentication(Agent.MINECRAFT);
		;
		//sessionService
//		for (Field field : Minecraft.getMinecraft().getClass().getDeclaredFields()) {
//			if(field.getType().equals(MinecraftSessionService.class)){
//				try {
//					//将字段的访问权限设为true：即去除private修饰符的影响
//					field.setAccessible(true);
//				/*去除final修饰符的影响，将字段设为可修改的*/
//					Field modifiersField = Field.class.getDeclaredField("modifiers");
//					modifiersField.setAccessible(true);
//					modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
//					field.set(Minecraft.getMinecraft(), authService.createMinecraftSessionService());
//				} catch (IllegalAccessException | NoSuchFieldException e) {
//					e.printStackTrace();
//				}
//
//				break;
//			}
//		}
	}

	public static AltManager getInstance() {
		if (manager == null) {
			manager = new AltManager();
		}

		return manager;
	}
	public void setSession(Session session){
		try {
			MR.setSession(session);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void setUserOffline(String username,String token) {
		this.auth.logOut();
		Session session = new Session(username, username, token, "legacy");
		try {
			MR.setSession(session);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
