package com.lalameow.serverlogin.third;

import com.lalameow.serverlogin.client.ClientProxy;
import com.mojang.authlib.AuthenticationService;
import com.mojang.authlib.BaseUserAuthentication;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.exceptions.AuthenticationException;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/15
 * 时间: 13:45
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class LaLaMeowAuthentication extends BaseUserAuthentication {
    protected LaLaMeowAuthentication(AuthenticationService authenticationService) {
        super(authenticationService);
    }

    @Override
    public void logIn() throws AuthenticationException {
    }

    @Override
    public boolean canPlayOnline() {
        return false;
    }

    @Override
    public GameProfile[] getAvailableProfiles() {
        return new GameProfile[0];
    }

    @Override
    public void selectGameProfile(GameProfile profile) throws AuthenticationException {

    }

    @Override
    public String getAuthenticatedToken() {
        return ClientProxy.clientId;
    }
}
