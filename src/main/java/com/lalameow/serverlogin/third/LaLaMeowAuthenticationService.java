package com.lalameow.serverlogin.third;

import com.mojang.authlib.Agent;
import com.mojang.authlib.BaseAuthenticationService;
import com.mojang.authlib.GameProfileRepository;
import com.mojang.authlib.UserAuthentication;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/15
 * 时间: 13:49
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class LaLaMeowAuthenticationService extends BaseAuthenticationService {
    public LaLaMeowAuthenticationService() {

    }

    @Override
    public UserAuthentication createUserAuthentication(Agent agent) {
        return new LaLaMeowAuthentication(this);
    }

    @Override
    public MinecraftSessionService createMinecraftSessionService() {
        return new LaLaMeowMinecraftSessionService(this);
    }

    @Override
    public GameProfileRepository createProfileRepository() {
        throw new UnsupportedOperationException("LaLaMeow authentication service has no profile repository");
    }
}
