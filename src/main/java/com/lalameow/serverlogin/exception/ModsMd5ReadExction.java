package com.lalameow.serverlogin.exception;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiErrorScreen;
import net.minecraftforge.fml.client.CustomModLoadingErrorDisplayException;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/11
 * 时间: 1:06
 * 功能：请进行修改
 */
public class ModsMd5ReadExction extends CustomModLoadingErrorDisplayException {
    private String message;
    public ModsMd5ReadExction(String message, Throwable cause) {
        super(message, cause);
        this.message=message;
    }

    @Override
    public void initGui(GuiErrorScreen errorScreen, FontRenderer fontRenderer) {

    }

    @Override
    public void drawScreen(GuiErrorScreen errorScreen, FontRenderer fontRenderer, int mouseRelX, int mouseRelY, float tickTime) {
        int halfScreenWidth = errorScreen.width / 2;
        int halfScreenHeight= errorScreen.height / 2;
        errorScreen.drawCenteredString(fontRenderer,message,halfScreenWidth,halfScreenHeight-40,0xe0e0e0);
    }
}
