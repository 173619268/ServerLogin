package com.lalameow.serverlogin.network;

import com.lalameow.serverlogin.api.NetClient;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/8
 * 时间: 3:23
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class MainClient implements Runnable{
    private NetClient mcLoginClient;

    public MainClient(NetClient mcLoginClient) {
        this.mcLoginClient = mcLoginClient;
    }

    @Override
    public void run() {
        mcLoginClient.initClient();

    }
}
