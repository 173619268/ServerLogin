package com.lalameow.serverlogin;

import com.lalameow.serverlogin.common.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/25
 * 时间: 3:25
 * 功能：请进行修改
 */
@Mod(modid = ServerLoginMod.MODID, name = ServerLoginMod.NAME, version = ServerLoginMod.VERSION)
public class ServerLoginMod {
    public static final String MODID = "serverloginmod";
    public static final String NAME = "Server Login Mod";
    public static final String VERSION = "1.2";

    @SidedProxy(clientSide = "com.lalameow.serverlogin.client.ClientProxy",
            serverSide = "com.lalameow.serverlogin.common.CommonProxy")
    public static CommonProxy proxy;

    @Mod.Instance(ServerLoginMod.MODID)
    public static ServerLoginMod instance;

    public static final String channel = "ServerLogin";

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        proxy.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);

    }
}
