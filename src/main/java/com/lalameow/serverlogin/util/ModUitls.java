package com.lalameow.serverlogin.util;

import com.lalameow.packet.pojo.MdsInfo;
import com.lalameow.serverlogin.exception.ModsMd5ReadExction;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.client.GuiCustomModLoadingErrorScreen;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import sun.applet.Main;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/10
 * 时间: 22:49
 * 功能：请进行修改
 */
@SideOnly(Side.CLIENT)
public class ModUitls {
    public static GuiCustomModLoadingErrorScreen errorScreen=null;
    private static List<MdsInfo> mdsInfos=new ArrayList<>();

    public static void scanFile() {
        modsRead();
        mcCoreFileRead();
    }
    public static MdsInfo[] getMdsArray(){
        MdsInfo[] mdsinfoArray=new MdsInfo[mdsInfos.size()];
        return mdsInfos.toArray(mdsinfoArray);
    }
    private static void mcCoreFileRead(){
        String versionspath=Minecraft.getMinecraft().mcDataDir.getAbsolutePath()+File.separator+"versions";
        File versionFile=new File(versionspath);
        if(versionFile.exists()){
            Collection<File> fileCollection=FileUtils.listFiles(versionFile,new String[]{"jar"},true);
            for (File file : fileCollection) {
                MdsInfo mdsInfo= getFileMd5(file);
                if(mdsInfo!=null){
                    mdsInfos.add(mdsInfo);
                }
            }
        }

    }

    private static void modsRead(){

        String modPath=Minecraft.getMinecraft().mcDataDir.getAbsolutePath()+File.separator+"mods";
        File ftpFold=new File(modPath);
        FileFilter fileFilter= new SuffixFileFilter(".jar");
        FileFilter litemodFilter= new SuffixFileFilter(".litemod");
        File[] litemodFile=ftpFold.listFiles(litemodFilter);
        File[] modsFile=ftpFold.listFiles(fileFilter);
        if(modsFile!=null){
            addModsInfo(modsFile);
        }
        if(litemodFile!=null){
            addModsInfo(litemodFile);
        }
    }
    private static void addModsInfo( File[] files){
        for (File file : files) {
            MdsInfo mdsInfo= getFileMd5(file);
            if(mdsInfo!=null){
                mdsInfos.add(mdsInfo);
            }
        }
    }
    private static MdsInfo getFileMd5(File file){
        FileInputStream fileInputStream = null;
        try {
            MdsInfo mdsInfo=new MdsInfo();
            fileInputStream=new FileInputStream(file);
            mdsInfo.setMdsMd5( DigestUtils.md5Hex(fileInputStream));
            mdsInfo.setMdsHash(DigestUtils.sha1Hex(fileInputStream));
            mdsInfo.setMdsName(file.getName());
            return mdsInfo;
        } catch (IOException e) {
            errorScreen=new GuiCustomModLoadingErrorScreen(new ModsMd5ReadExction(e.getMessage(),e));
        }finally {
            IOUtils.closeQuietly(fileInputStream);
        }
        return null;

    }

}
