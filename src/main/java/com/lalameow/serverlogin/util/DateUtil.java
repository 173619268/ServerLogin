package com.lalameow.serverlogin.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author 陈刚
 * @version 1.0
 * @project ZNSM
 * @package com.haorui.baseutil.utils
 * @createDate 2017-09-16 14:15
 * @modfiyDate
 * @function
 */
public class DateUtil {
    // 以毫秒表示的时间
    private static final long DAY_IN_MILLIS = 24 * 3600 * 1000;
    private static final long HOUR_IN_MILLIS = 3600 * 1000;
    private static final long MINUTE_IN_MILLIS = 60 * 1000;
    private static final long SECOND_IN_MILLIS = 1000;

    /**
     * 时间添加
     * @param operDate 需要操作的日期
     * @param flag 单位
     * @param addCount 数量
     * @return
     */
    public static Date dateAdd(Date operDate,char flag,int addCount){
        Calendar calendar=getCalendar(operDate);
        switch (flag){
            case 'y':
                calendar.add(Calendar.YEAR,addCount);
                return calendar.getTime();
            case 'M':
                calendar.add(Calendar.MONTH,addCount);
                return calendar.getTime();
            case 'd':
                calendar.add(Calendar.DAY_OF_MONTH,addCount);
                return calendar.getTime();
            case 'h':
                calendar.add(Calendar.HOUR,addCount);
                return calendar.getTime();
            case 'm':
                calendar.add(Calendar.MINUTE,addCount);
                return calendar.getTime();
            case 's':
                calendar.add(Calendar.SECOND,addCount);
                return calendar.getTime();
            default:

                break;
        }
        return null;
    }

    public static Date getNowDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        Date currentTime_2 = null;
        try {
            currentTime_2 = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentTime_2;
    }

    /**
     * 获取当前日期
     * 格式：yyyy-MM-dd HH:mm:ss
     * @return String
     */
    public static String getNowDateString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(new Date());
        return  dateString;
    }
    public static String getNowDateYYMMDDhhmmss(){
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        return formatter.format(currentTime);
    }

    /**
     * 字符串转date（带时分秒）
     * @param dateStr
     * @return
     */
    public static Date str2DateHMS(String dateStr){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return str2Date(dateStr,simpleDateFormat);
    }

    /**
     * 字符串转date（不带时分秒）
     * @param dateStr
     * @return
     */
    public static Date str2DateNOHMS(String dateStr){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return str2Date(dateStr,simpleDateFormat);
    }

    /**
     * 字符串转换成日期
     * @param dateStr
     * @param formate
     * @return
     */
    public static Date str2Date(String dateStr,String formate){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formate);
        return str2Date(dateStr,simpleDateFormat);
    }
    /**
     * 字符串转换成日期
     * @param str
     * @param sdf
     * @return
     */
    public static Date str2Date(String str, SimpleDateFormat sdf) {
        if (null == str || "".equals(str)) {
            return null;
        }
        Date date = null;
        try {
            date = sdf.parse(str);
            return date;
        } catch (ParseException e) {
            try {
                Long mils=Long.parseLong(str);
                Calendar calendar=Calendar.getInstance();
                calendar.setTimeInMillis(mils);
                return calendar.getTime();
            }catch (Exception e2){
                e.printStackTrace();
                e2.printStackTrace();
            }
        }
        return null;
    }

    /**
     *
     * @param flag 单位 年:y;天:d;小时:h;分钟:m;秒:s
     * @param bigerDate
     * @param smallerDate
     * @return
     */
    public static int dateDiff(char flag,Date bigerDate,Date smallerDate){
        return dateDiff(flag, getCalendar(bigerDate.getTime()),getCalendar(smallerDate.getTime()));
    }
    /**
     * 计算两时间之间的差值
     * @param flag 单位 年:y;天:d;小时:h;分钟:m;秒:s
     * @param bigerDate 时间晚的日期
     * @param smallerDate 时间早的日期
     * @return
     */
    public static int dateDiff(char flag, Calendar bigerDate, Calendar smallerDate) {

        long millisDiff = getMillis(bigerDate) - getMillis(smallerDate);
        if (flag == 'y') {
            return (bigerDate.get(Calendar.YEAR) - smallerDate.get(Calendar.YEAR));
        }

        if (flag == 'd') {
            return (int) (millisDiff / DAY_IN_MILLIS);
        }

        if (flag == 'h') {
            return (int) (millisDiff / HOUR_IN_MILLIS);
        }

        if (flag == 'm') {
            return (int) (millisDiff / MINUTE_IN_MILLIS);
        }

        if (flag == 's') {
            return (int) (millisDiff / SECOND_IN_MILLIS);
        }

        return 0;
    }
    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
    public static Calendar getCalendar(long millis) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(millis));
        return cal;
    }
    /**
     * 系统时间的毫秒数
     *
     * @return 系统时间的毫秒数
     */
    public static long getMillis() {
        return System.currentTimeMillis();
    }

    /**
     * 指定日历的毫秒数
     *
     * @param cal
     *            指定日历
     * @return 指定日历的毫秒数
     */
    public static long getMillis(Calendar cal) {
        return cal.getTime().getTime();
    }

    /**
     * 指定日期的毫秒数
     *
     * @param date
     *            指定日期
     * @return 指定日期的毫秒数
     */
    public static long getMillis(Date date) {
        return date.getTime();
    }

    /**
     * 指定时间戳的毫秒数
     *
     * @param ts
     *            指定时间戳
     * @return 指定时间戳的毫秒数
     */
    public static long getMillis(Timestamp ts) {
        return ts.getTime();
    }
    /**
     * 将java.util.Date 格式转换为字符串格式'yyyy-MM-dd'(24小时制)<br>
     * 如Sat May 11 17:24:21 CST 2002 to '2002-05-11 17:24:21'<br>
     * @param time Date 日期<br>
     * @return String   字符串<br>
     */
    public static String dateToString(Date time){
        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat ("yyyy-MM-dd");
        String ctime = formatter.format(time);
        return ctime;
    }
    /**
     * 将java.util.Date 格式转换为字符串格式'yyyyMMdd'(24小时制)<br>
     * 如Sat May 11 17:24:21 CST 2002 to '2002-05-11 17:24:21'<br>
     * @param time Date 日期<br>
     * @return String   字符串<br>
     */
    public static String dateToString2(Date time){
        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat ("yyyyMMdd");
        String ctime = formatter.format(time);
        return ctime;
    }
    /**
     * 获取本月第一天
     */
    public static Date getCalendarFristDay() {
        //获取当前月第一天：
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        Date currentTime = c.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        Date currentTime_2 = null;
        try {
            currentTime_2 = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentTime_2;
    }
    /**
     * 获取上个月月第一天
     */
    public static Date getlastMontyStart() {
        //获取当前月第一天：
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        c.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        Date currentTime = c.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        Date currentTime_2 = null;
        try {
            currentTime_2 = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentTime_2;
    }
    /**
     * 获取本月最后一天
     */
    public static Date getCalendarLastDay() {
        //获取当前月最后一天
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
        ca.set(Calendar.HOUR_OF_DAY, 23);
        ca.set(Calendar.MINUTE, 59);
        ca.set(Calendar.SECOND, 59);
        Date currentTime = ca.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        Date currentTime_2 = null;
        try {
            currentTime_2 = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentTime_2;


    }

    public static Date getWeekStartDate(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        Date currentTime = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);

        Date currentTime_2 = null;
        try {
            currentTime_2 = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentTime_2;
    }

    /**
     * 得到今年的第一天
     * @return
     */
    public  static  Date getyearStartDate()
    {
        Calendar currCal=Calendar.getInstance();
        int currentYear = currCal.get(Calendar.YEAR);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, currentYear);
        calendar.set(Calendar.MONTH,0);
        calendar.set(Calendar.DATE,1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date currYearFirst = calendar.getTime();
        return currYearFirst;
    }
    /**
     * 得到今年的最后一天
     * @return
     */
    public  static  Date getyearEndDate()
    {
        Calendar currCal=Calendar.getInstance();
        int currentYear = currCal.get(Calendar.YEAR);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, currentYear);
        calendar.set(Calendar.MONTH,12);
        calendar.set(Calendar.DATE,31);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date currYearFirst = calendar.getTime();
        return currYearFirst;
    }
    /**
     * 得到今年的第一天
     * @return
     */
    public  static  String getyearStartDateString()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(getyearStartDate());
        return dateString;
    }
    /**
     * 得到今年的第一天
     * @return
     */
    public  static  String getyearEndDateString()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(getyearEndDate());
        return dateString;
    }


    public  static  String getMonthStartDateString()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(getCalendarFristDay());
        return  dateString;
    }

    /**
     * 得到上个月第一天
     * @return
     */
    public  static  String getLastMonthStartDateString()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(getlastMontyStart());
        return  dateString;
    }

    /**
     * 获取当前日期周一日期
     * @return
     */
    public static String getWeekStartDateString(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(getWeekStartDate());;
        return dateString;
    }

    /**
     * 获取当前周是当前日期年的第几周
     * @return
     */
    public  static  int getthisyearofweek()
    {
        int week=0;
        Calendar calendar = Calendar.getInstance();
        week=calendar.get(Calendar.WEEK_OF_YEAR);
        return week;
    }

    /**
     * 获取当前时间的年份
     * @return
     */
    public  static  int getthisyearofyear()
    {
        int year=0;
        Calendar calendar = Calendar.getInstance();
        year=calendar.get(Calendar.YEAR);
        return year;
    }

    public  static  int getthisyearofmonth()
    {
        int month=0;
        Calendar calendar = Calendar.getInstance();
        month=calendar.get(Calendar.MONTH)+1;
        return month;
    }
    /**
     * 比较时间
     * @return
     */
    public static boolean datecomp(String begintime,String endtime){
        Calendar calendar=DateUtil.getCalendar(new Date());
        long dqTime=calendar.getTimeInMillis();
        long benginTime=-1;
        long endTiem=-1;
        String[] bengTemp=begintime.split(":");
        calendar.set(Calendar.HOUR_OF_DAY,Integer.parseInt(bengTemp[0]));
        calendar.set(Calendar.MINUTE,Integer.parseInt(bengTemp[1]));
        calendar.set(Calendar.SECOND,Integer.parseInt(bengTemp[2]));
        benginTime=calendar.getTimeInMillis();
        String[] endTemp=endtime.split(":");
        calendar.set(Calendar.HOUR_OF_DAY,Integer.parseInt(endTemp[0]));
        calendar.set(Calendar.MINUTE,Integer.parseInt(endTemp[1]));
        calendar.set(Calendar.SECOND,Integer.parseInt(endTemp[2]));
        endTiem=calendar.getTimeInMillis();

        return (dqTime>benginTime && dqTime<endTiem);
//
    }

}
