package com.lalameow.packet;

import com.lalameow.packet.enumtype.PackType;

import java.io.Serializable;
import java.security.Key;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/7
 * 时间: 21:14
 * 功能：请进行修改
 */
public class BasePack implements Serializable {
    //必须唯一，否者会出现channel调用混乱
    private String clientId;
    private PackType packType;
    private String bodyContent;
    private Key key;
    private String header;

    @Override
    public String toString() {
        return "BasePack{" +
                "clientId='" + clientId + '\'' +
                ", packType=" + packType +
                ", bodyContent='" + bodyContent + '\'' +
                ", key='" + key + '\'' +
                ", header='" + header + '\'' +
                '}';
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public PackType getPackType() {
        return packType;
    }

    public void setPackType(PackType packType) {
        this.packType = packType;
    }

    public String getBodyContent() {
        return bodyContent;
    }

    public void setBodyContent(String bodyContent) {
        this.bodyContent = bodyContent;
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
}
