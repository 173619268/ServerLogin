package com.lalameow.packet.pojo;


/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/10
 * 时间: 21:07
 * 功能：请进行修改
 */
public class MdsInfo {
    private String mdsName;
    private String mdsMd5;
    private String mdsHash;

    public String getMdsName() {
        return mdsName;
    }

    public void setMdsName(String mdsName) {
        this.mdsName = mdsName;
    }

    public String getMdsMd5() {
        return mdsMd5;
    }

    public void setMdsMd5(String mdsMd5) {
        this.mdsMd5 = mdsMd5;
    }

    public String getMdsHash() {
        return mdsHash;
    }

    public void setMdsHash(String mdsHash) {
        this.mdsHash = mdsHash;
    }
}
