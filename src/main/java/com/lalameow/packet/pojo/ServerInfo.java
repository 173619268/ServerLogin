package com.lalameow.packet.pojo;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/8
 * 时间: 4:10
 * 功能：请进行修改
 */
public class ServerInfo {
    private String serverHost;
    private String serverName;

    public String getServerHost() {
        return serverHost;
    }

    public void setServerHost(String serverHost) {
        this.serverHost = serverHost;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }
}
