package com.lalameow.packet.enumtype;

import com.google.gson.Gson;
import com.lalameow.packet.PackInterface;
import com.lalameow.packet.packImpl.*;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/7
 * 时间: 21:15
 * 功能：请进行修改
 */
public enum  PackType {
    PING(PingPack.class),
    LOGIN(LoginPack.class),
    LOGINRECIVE(LoginRecivePack.class),
    REGIST(RegistPack.class),
    REGISTRECIVE(RegistRecivePack.class),
    EDITPSW(EditPswPack.class),
    EDITPSWRECIVE(EditPswRecivePack.class),
    VAILDCLIENT(null);

    private Class<? extends PacketAbstract> packetClass;
    PackType(Class<? extends PacketAbstract> packetClass){
        this.packetClass =packetClass;
    }

    public Class<? extends PacketAbstract> getPacketClass() {
        return packetClass;
    }

    /**
     * 获取json
     * @param packInterface
     * @return
     */
    public String getJsonContent(PackInterface packInterface){
        return  packInterface.toJsonStr();
    }

    /**
     * 获取对象
     * @param json
     * @param <T>
     * @return
     */
    public PacketAbstract getObject(String json){
        if(this.getPacketClass()!=null){
            Gson gson=new Gson();
            return  gson.fromJson(json, this.getPacketClass());
        }else {
            return null;
        }

    }
}
