package com.lalameow.packet.packImpl;

import com.lalameow.packet.BasePack;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/26
 * 时间: 9:52
 * 功能：请进行修改
 */
public class EditPswPack extends PacketAbstract{
    private String username;
    private String oldPwd;
    private String newPwd;

    public String getOldPwd() {
        return oldPwd;
    }

    public void setOldPwd(String oldPwd) {
        this.oldPwd = oldPwd;
    }

    public String getNewPwd() {
        return newPwd;
    }

    public void setNewPwd(String newPwd) {
        this.newPwd = newPwd;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public BasePack revice() {
        return null;
    }
}
