package com.lalameow.packet.packImpl;


import com.lalameow.packet.BasePack;
import com.lalameow.packet.pojo.ServerInfo;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/8
 * 时间: 0:08
 * 功能：请进行修改
 */
public class LoginRecivePack extends PacketAbstract {
    private boolean login;
    private String msg;
    private ServerInfo[] serverInfoArray;
    private String playerName;

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ServerInfo[] getServerInfoArray() {
        return serverInfoArray;
    }

    public void setServerInfoArray(ServerInfo[] serverInfoArray) {
        this.serverInfoArray = serverInfoArray;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    @Override
    public BasePack revice() {

        return null;
    }
}
