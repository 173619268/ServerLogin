package com.lalameow.packet.packImpl;


import com.google.gson.Gson;
import com.lalameow.packet.BasePack;
import com.lalameow.packet.PackInterface;
import com.lalameow.packet.pojo.MdsInfo;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/26
 * 时间: 9:52
 * 功能：请进行修改
 */
public class LoginPack extends PacketAbstract {
    private String userName;
    private String password;
    private MdsInfo[] mdsInfos;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MdsInfo[] getMdsInfos() {
        return mdsInfos;
    }

    public void setMdsInfos(MdsInfo[] mdsInfos) {
        this.mdsInfos = mdsInfos;
    }

    @Override
    public BasePack revice() {

        return null;
    }
}
