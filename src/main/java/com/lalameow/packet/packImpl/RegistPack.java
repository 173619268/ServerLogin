package com.lalameow.packet.packImpl;

import com.lalameow.packet.BasePack;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/2/26
 * 时间: 9:52
 * 功能：请进行修改
 */
public class RegistPack extends PacketAbstract {
    private String playerName;
    private String userName;
    private String password;
    private String qq;
    private String tuijianren;
    private String code;
    private long timespan;
    private String registDate;
    private String mac;
    private String machineCode;

    @Override
    public BasePack revice() {
        return null;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getTuijianren() {
        return tuijianren;
    }

    public void setTuijianren(String tuijianren) {
        this.tuijianren = tuijianren;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getTimespan() {
        return timespan;
    }

    public void setTimespan(long timespan) {
        this.timespan = timespan;
    }

    public String getRegistDate() {
        return registDate;
    }

    public void setRegistDate(String registDate) {
        this.registDate = registDate;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getMachineCode() {
        return machineCode;
    }

    public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }
}
