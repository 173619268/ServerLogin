package com.lalameow.packet.packImpl;

import com.lalameow.packet.BasePack;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/8
 * 时间: 5:36
 * 功能：请进行修改
 */
public class RegistRecivePack extends PacketAbstract {
    private boolean registOk;
    private String msg;

    public boolean isRegistOk() {
        return registOk;
    }

    public void setRegistOk(boolean registOk) {
        this.registOk = registOk;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public BasePack revice() {
        return null;
    }
}
