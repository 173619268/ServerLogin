package com.lalameow.packet.packImpl;

import com.lalameow.packet.BasePack;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/8
 * 时间: 5:37
 * 功能：请进行修改
 */
public class EditPswRecivePack extends PacketAbstract{
    private boolean editOk;
    private String msg;

    public boolean isEditOk() {
        return editOk;
    }

    public void setEditOk(boolean editOk) {
        this.editOk = editOk;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public BasePack revice() {
        return null;
    }
}
